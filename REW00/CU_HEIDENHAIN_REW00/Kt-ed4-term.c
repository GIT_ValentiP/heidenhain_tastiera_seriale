/**********************************************************************
;                                                                     *
;    Filename:	    Kt-ed4-term.c	(rifacimento terminale seriale, da versione Boagno, x K-Tronic)*
;    Date:          26/7/11																						*
;    File Version:  1.00                                              *
;                                                                     *
;    Author:        Dario Greggio                                     *
;    Company:       Cyberdyne (ADPM Synthesis sas)                    *
;                                                                     *
;**********************************************************************
;                                                                     *
;    Notes:         
;				26 luglio 2011, dopo la sicilia (e Daniela?)

;                                                                     *
;**********************************************************************
;                                                                     *
;*********************************************************************/


#include <p18cxxx.h>        // processor specific variable definitions

#include <timers.h>
#include <usart.h>
#include <portb.h>
#include <reset.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <delays.h>
#include <GenericTypeDefs.h>
#include "Kt-ed4-term.h"



#ifdef __18F46K22

#pragma config WDTEN = ON, WDTPS = 32768, MCLRE=EXTMCLR, STVREN=ON, LVP=OFF
#pragma config FOSC=HSHP, PLLCFG = ON, PRICLKEN = ON, FCMEN=OFF, IESO=OFF, PWRTEN=ON, BOREN=SBORDIS, BORV=285
#pragma config PBADEN=OFF, CCP2MX=PORTC1, XINST=ON, DEBUG=OFF

#endif

#ifdef __18F4520

#pragma config WDT = ON, WDTPS = 32768, MCLRE=ON, STVREN=ON, LVP=OFF
#pragma config OSC=HSPLL, FCMEN=OFF, IESO=OFF, PWRT=ON, BOREN=SBORDIS, BORV=2
#pragma config LPT1OSC=OFF, PBADEN=OFF, CCP2MX=PORTC, XINST=OFF, DEBUG=OFF

#endif

#ifdef __18F4550

// 4MHz

#pragma config WDT = ON, WDTPS = 1024, MCLRE=ON, STVREN=ON, LVP=OFF
//#pragma config WDT = ON, WDTPS = 16384, MCLRE=ON, STVREN=ON, LVP=OFF
#pragma config PLLDIV=1, CPUDIV=OSC1_PLL2, USBDIV=2, FOSC=HSPLL_HS, IESO=OFF, PWRT=OFF, BOR=ON, BORV=2
#ifndef __EXTENDED18__
#pragma config VREGEN=OFF, LPT1OSC=OFF, PBADEN=OFF, CCP2MX=ON, XINST=OFF, DEBUG=OFF
#else
#pragma config VREGEN=OFF, LPT1OSC=OFF, PBADEN=OFF, CCP2MX=ON, XINST=ON, DEBUG=OFF
#endif
#endif



enum {
	ATTESA_1BYTE,
	ATTESA_2BYTE,
	ATTESA_3BYTE,
	ATTESA_END
	};

enum {
	LED_SPENTO=1,
	LED_ACCESO=2,
	LED_BLINK=3
	};

#pragma romdata myidlocs=0x200000
const rom char data0=0x4u;
const rom char data1=0x7u;
const rom char data2=0x4u;
const rom char data3=0x4u;
const rom char data4=0x00u;
const rom char data5=0x00u;
const rom char data6=0x00u;
const rom char data7=0x00u;
#pragma romdata


// 135nS/istruzione @ 29,4912MHz (7.3728 + PLL x4)


//
//***** VARIABLE DEFINITIONS

static rom const char CopyrightString[]= {'P','I','C','K','T','-','e','d','4','-','t','e','r','m',' ','f','/','w',' ',
	VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0', ' ','2','8','/','0','7','/','1','1', 0 };

static rom const char CopyrDate[]={ ' ','-',' ','S','/','N',' ','0','0','0','1', CR,LF,0 };

static rom const char * rom Copyr1="(C) xxxxxxxxx xxxx - xxxxx\xx\xx\xx";

#pragma udata

//

//
BYTE InitOK;

BYTE CounterL2,CounterL,CounterH;	
//volatile BYTE Timer1;
volatile BYTE Timer10Sec,Timer1Sec;

BYTE ByteCnt;      // lunghezza del pacchetto ricevuto
BYTE CommTimer;		// timeout in ricezione


//#pragma udata access ACCESSBANK
#pragma udata 

volatile /*near*/ BYTE Buf232Ptr,Buf232PtrI;
volatile /*near*/ BYTE Buf232[BUF_232_SIZE];
volatile /*near*/ struct COMM_STATUS CommStatus;


#pragma udata

BYTE TimerBuzz;
BYTE Timer2;					// usato come divisore per generare gli eventi "controllo presenza 220" e "timed notify"


BYTE packetOut[LEN_FRAME_TX_CU_LIGI];
BYTE Comando[4];

BYTE FLAGS;
BYTE kbLayout=0;


BYTE temp;			// temporanea

//bank 0 & mirrored in EEPROM!
BYTE State,oldState;

BYTE statoLed[11]={LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO,LED_SPENTO};
WORD statoTasti;
WORD statoTastiPrec;
BYTE statoBuzzer=LED_SPENTO;

BYTE num_all_blink=0; 


void _isr(void);
void _low_isr(void);
void timer_isr(void);
void usart_isr(void);

#define Delay08()	Delay_uS(TIME_GRANULARITY)			// 1 bit-time 
#define Delay_1uS() Delay_uS(1)




void Delay_mS(BYTE);
void Delay_uS(BYTE);
void Delay_S_(BYTE);
#define Delay_S() Delay_S_(10)				// circa 1s 

#define EEscrivi0(p) EEscrivi(p,0)               // scrive 0...

#define EEscrivi(a,b) { *((BYTE *)a)=b; EEscrivi_(a,b); }  // scrive W in RAM e EPROM...

void EEscrivi_(SHORTPTR,BYTE);
BYTE EEleggi(SHORTPTR);
#define EEcopiaARAM(p) { *p=EEleggi(p); }
#define EEcopiaAEEPROM(p) EEscrivi_(p,*p)

void StdBeep(void);
void ErrorBeep(void);
void ConfirmBeep(void);
void lampLed(void);
void gestisciLed(void);
void AnimationLedsInit(void);
void AllLedOn(void);
void AllLedOff(void);

short int GetByte(void);
//char isRecv232(void);
/*char isRecv232(void) {

	return Buf232Ptr != Buf232PtrI;
	}*/
#define isRecv232() (Buf232Ptr != Buf232PtrI)
BYTE PutByte(BYTE);


BYTE SpedisciFrame(WORD);
signed char EseguiComando(BYTE *);


/****************************************************************************
*
*  high_interrupt
*  Descrizione: gestore dell'interrupt
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:
*
****************************************************************************/

#pragma code high_vector=0x8		// solo HIGH, lavorando in Compatibility Mode
void interrupt_at_high_vector(void) {
  _asm 
	goto _isr
	_endasm
	}
#pragma code

#pragma code low_vector=0x18
void interrupt_at_low_vector(void) {
  _asm goto _low_isr _endasm
	}


#pragma code
#pragma interrupt _isr
//#pragma interrupt high_isr nosave=section("MATH_DATA"),section(".tmpdata"),PRODH
void _isr(void) {
	
	usart_isr();
	}

#pragma interruptlow _low_isr
void _low_isr(void) {
	
	timer_isr();
	}

/****************************************************************************
*
*  timer_isr
*  Descrizione: gestore dell'interrupt del Timer 0
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:	100mSec
*
****************************************************************************/
void timer_isr(void) {

	INTCONbits.TMR0IF = 0;
	WriteTimer0(TMR0BASE);			// re-inizializzo TMR0
	

//	Timer1++;
//	if(Timer1 & 0x10) {

//		LATCbits.LATC1 ^= 1;			// 100mSec ca.

//		Timer1=0;

// avendo ClrWdt(); nell'IRQ, in pratica non salta MAI!!

		Timer10Sec++;

		if(!(Timer10Sec % 10)) {
			if(Timer1Sec)
				Timer1Sec--;
			}


//		}

	}



void usart_isr(void) {
	static unsigned char byteRec,temp;


		LATCbits.LATC1 ^=1;			// DEBUG ***


//	if(/*PIR1bits.TXIF || */
//		PIE1bits.RCIE && PIR1bits.RCIF) {		// serve il controllo su RCIE, se abiliti e disabiliti IRQ da programma...

//		ClrWdt();
	
	//	bcf  PIR1,RCIF
	
		temp=RCSTA;								//salvare RCSTA PRIMA di leggere RCREG (v. datasheet) e tempC e' usato da GetParita
	
		if(RCSTAbits.FERR)
			CommStatus.COMM_FRERR=1;
		if(RCSTAbits.OERR) {
			RCSTAbits.CREN=0;
			Nop(); Nop();
	//		Delay10TCY();
			RCSTAbits.CREN=1;								// forse basta BCF (v. datasheet), anche se non e' chiaro...
			CommStatus.COMM_OVRERR=1;
			}

	
		byteRec=RCREG /* ReadUSART() */;			// QUESTO pulisce interrupt!
																	// salvo cmq anche qui...
		Buf232[Buf232Ptr++]=byteRec;				// max 32
		Buf232Ptr &= (BUF_232_SIZE-1);

//		}


	}



// remaining code goes here

#pragma code


/****************************************************************************
*
*  main
*  Descrizione:
*  Riferimento:
*  Parametri in ingresso: nessuno
*  Parametri in uscita: nessuno
*  Note:
*
****************************************************************************/
void main(void) {
	BYTE *p,myBuf[8];
	BYTE i;
	char ch;
    BYTE blink_counter=0;   
start:
cold_reset:
	ClrWdt();				// DOPO!

//	RCONbits.NOT_BOR=1;
	StatusReset();



	LATA=0b00000001;			// led attivo basso, RTS attivo alto ??
	LATB=0b01000010;			// led attivo basso
	LATC=0b00000000;			// CCP1=0 x buzzer, RS232
	LATD=0b10011001;			// led attivi bassi
	LATE=0b00000111;			// led attivi bassi

	
	ADCON0=0b00000000;	// tanto per... Disattivato.
	ADCON1=0b00000000;	// 
	ADCON2=0b00000000;	// 

	ANSELA=0b00000000;
	ANSELB=0b00000000;			// inutile se c'� PBADEN...
	ANSELC=0b00000000;		// funz analogiche su PORTC
	ANSELD=0b00000000;		// idem
	ANSELE=0b00000000;		// idem
	
	TRISA=0b00111110;				// 1 output, 5 input, OSC
	TRISB=0b10111101;				// 2 output, 6 input
	TRISC=0b11100000;				// 1=input, 0=output (I2C), 0=output per PWM! 1 x USART
	TRISD=0b01100110;				// 4 output, 4 input
//	TRISD=0b00000000;				// 4 output, 4 input  con questa non andavano TAST10 - TAST11  -  TAST12 - TAST13
	TRISE=0b01100000;				// 3 output
//	TRISE=0b01100110;				// 3 output  con questa non andavano LED07 - LED08  


	TMR0L=TMR0H=0;				//clr tmr0 & prescaler

//	movlw	00000111b	; pull-ups B, tmr0 enable, prescaler Timer with 1:256 prescaler FINIRE!!
//	movwf	OPTION_REG

	INTCON=0;			// disabilito tutti interrupt

//	OpenRB0INT(PORTB_CHANGE_INT_OFF & FALLING_EDGE_INT & PORTB_PULLUPS_ON);
	EnablePullups();


	INTCON2bits.TMR0IP = 0;			// Timer-0 Low Pty
	INTCON2bits.RBIP = 0;				// PortB (non usato) Low Pty
	IPR1bits.RCIP = 1;					// USART High Pty
	RCONbits.IPEN=1;				// interrupt in Modalita' Avanzata (18X)

	OpenTimer0(TIMER_INT_ON & T0_16BIT & T0_SOURCE_INT & T0_PS_1_32);


// x=FOSC/BAUD_RATE/64 - 1  opp. 16 opp. 4
#if BAUD_RATE==4800
	Open1USART(USART_RX_INT_ON & USART_TX_INT_OFF & USART_ASYNCH_MODE & 
		USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH,
		1535 /*4800 : 29,4912/(4 * ( 1535 )) */);
	baud1USART(BAUD_16_BIT_RATE & BAUD_WAKEUP_OFF & BAUD_AUTO_OFF & BAUD_IDLE_RX_PIN_STATE_HIGH & BAUD_IDLE_TX_PIN_STATE_HIGH);
#elif BAUD_RATE==9600
	Open1USART(USART_RX_INT_ON & USART_TX_INT_OFF & USART_ASYNCH_MODE & 
		USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH,
		767 /*9600 : 29,4912/(4 * ( 767 )) */);
	baud1USART(BAUD_16_BIT_RATE & BAUD_WAKEUP_OFF & BAUD_AUTO_OFF & BAUD_IDLE_RX_PIN_STATE_HIGH & BAUD_IDLE_TX_PIN_STATE_HIGH);
#elif BAUD_RATE==19200
	Open1USART(USART_RX_INT_ON & USART_TX_INT_OFF & USART_ASYNCH_MODE & 
		USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH,
		383 /*19200 : 29,4912/(4 * ( 383 )) */);
	baud1USART(BAUD_16_BIT_RATE & BAUD_WAKEUP_OFF & BAUD_AUTO_OFF & BAUD_IDLE_RX_PIN_STATE_HIGH & BAUD_IDLE_TX_PIN_STATE_HIGH);
#elif BAUD_RATE==38400
	Open1USART(USART_RX_INT_ON & USART_TX_INT_OFF & USART_ASYNCH_MODE & 
		USART_EIGHT_BIT & USART_CONT_RX & USART_BRGH_HIGH,
		191 /*38400 : 29,4912/(4 * ( 191 )) */);
	baud1USART(BAUD_16_BIT_RATE & BAUD_WAKEUP_OFF & BAUD_AUTO_OFF & BAUD_IDLE_RX_PIN_STATE_HIGH & BAUD_IDLE_TX_PIN_STATE_HIGH);
#else
#error BAUD_RATE non supportato!
#endif


	Buf232Ptr=Buf232PtrI=0;

	ClrWdt();


	
	WriteTimer0(TMR0BASE);			// inizializzo TMR0

	T1CON=0b00110000;				// spengo Timer1
	CCP1CON=0b00001100;							// TMR2: PWM mode
//	CCP2CON=0x0c;								// TMR2: PWM mode
	CCPR1L=BEEP_STD_FREQ/2;
//	CCPR2L=BEEP_STD_FREQ/2;
//	CCPR1H=BEEP_STD_FREQ/2;
	T2CON=0b00000011;									// set prescaler T2
	// OpenPWM1(); USARE??
	PR2=BEEP_STD_FREQ;

//	EEcopiaARAM(&MyHouse);



	INTCONbits.GIE=1;				// attivo IRQ
	INTCONbits.PEIE=1;			// attivo IRQ


	oldState=State;


	ClrWdt();


debug_loop:


no_debug_init:

	statoTasti=0;
	statoTastiPrec=0;


	Delay_S_(5);						// se ci fosse display, mettere ritardo

  Timer2=240/FLASH_TIME;			// sub-timer


warm_reset:
	ClrWdt();							// rientro caldo (da watchdog):

	INTCONbits.GIE=1;				// attivo IRQ
	INTCONbits.PEIE=1;			// attivo IRQ
	INTCONbits.TMR0IE=1;


#ifdef DEBUG_TEST
	if(PORTB != 0xff) {
    for(i=0; i<3; i++) { 
			putrs1USART("test123\n");			// debug
			}
		}
#endif

	AnimationLedsInit();//lampLed();
  
	State=ATTESA_1BYTE;
	CounterL2=REPEAT_CYCLE_RX;
	CommTimer=0;


	do {

		ClrWdt();


		CounterL2--;
		if(!CounterL2) {	        // (sub-contatore: divide x n rispetto a STModem!)

			CounterL2=REPEAT_CYCLE_RX;


			if(isRecv232()) {
				ch=GetByte();

				switch(State) {

					case ATTESA_1BYTE:
						CommTimer=4*(FLASH_TIME+1);    // imposto il time-out a 2 giri di FLASH_TIME (ca. 100 mSec, )
						ch=toupper(ch);
						if(ch=='A' || ch=='S' || ch=='L') {
							Comando[0]=ch;
							State++;
							}
						break;
					
					case ATTESA_2BYTE:
						Comando[1]=ch;
						State++;			//
						break;

					case ATTESA_3BYTE:
						Comando[2]=ch;
						State++;			//pleonastico... :)

					case ATTESA_END:
						EseguiComando(Comando);

						State=ATTESA_1BYTE;
							
						Comando[0]=Comando[1]=Comando[2]=0;
						CommTimer=0;
						break;
					}
				}		// if received char



			CounterL--;

			if(!CounterL) {         // quando il contatore e' zero...


				CounterH--;


				if(!(CounterH & FLASH_TIME))
					goto ResCnt;
				
				}  // counterL

noComRic6:
			;
			}  // counterL2



	  continue;

	
ResCnt:
		if(CommTimer) {
			CommTimer--;
			if(!CommTimer) {
errore_ricezione:
				State=ATTESA_1BYTE;
				Comando[0]=Comando[1]=Comando[2]=0;
				CommTimer=0;
				}
			}


		statoTasti =  !m_Puls1Bit ? 1 : 0;			// leggo pulsanti 
		statoTasti |= !m_Puls15Bit ? 0b0100000000000010 : 0;//!m_Puls2Bit ? 0b0000000000000010 : 0;
		statoTasti |= !m_Puls3Bit ? 0b0000000000000100 : 0;
		statoTasti |= !m_Puls4Bit ? 0b0000000000001000 : 0;
		statoTasti |= !m_Puls5Bit ? 0b0000000000010000 : 0;
		statoTasti |= !m_Puls6Bit ? 0b0000000000100000 : 0;
		statoTasti |= !m_Puls7Bit ? 0b0000000001000000 : 0;
		statoTasti |= !m_Puls8Bit ? 0b0000000010000000 : 0;

		statoTasti |= !m_Puls9Bit  ? 0b0000000100000000 : 0;
		statoTasti |= !m_Puls10Bit ? 0b0000001000000000 : 0;
		statoTasti |= !m_Puls11Bit ? 0b0000010000000000 : 0;
		statoTasti |= !m_Puls12Bit ? 0b0000100000000000 : 0;
		statoTasti |= !m_Puls13Bit ? 0b0001000000000000 : 0;
		statoTasti |= !m_Puls14Bit ? 0b0010000000000000 : 0;// tasto numero 14//statoTasti |= !m_Puls14Bit ? 0b0010000000000000 : 0;//
		statoTasti |= !m_Puls2Bit ? 0b0100000000000000 : 0;//statoTasti |= !m_Puls15Bit ? 0b0100000000000000 : 0;
		statoTasti |= !m_Puls16Bit ? 0b1000000000000000 : 0;

//		if(statoTasti != statoTastiPrec) {
			SpedisciFrame(statoTasti);
			statoTastiPrec=statoTasti;						// salvo situazione input
//			}


// eventi gestiti da Timer2 (ogni tot secondi) ------------------------------------------
		switch(Timer2) {
			case 0:
				Timer2=240/FLASH_TIME;   // sub-timer... OCCHIO questa roba dev'essere DOPO ogni uso di Timer2...
				break;

			case 2:				// un po' di ritardo tra di loro...

				break;

			}


        if (blink_counter==0){
		   m_LedOBit ^= 1;		//125mS circa, 27/7/2011
           blink_counter=PERIOD_BLINK_LED_ITERATION;
        }else{
            blink_counter--;
        }   
        
		gestisciLed();
        
		Timer2--;
#ifdef DEBUG_VALENTI
        m_Led1Bit ^= 1; //Test duration cycle   
#endif
		} while(1);


	Close1USART();

	CloseTimer0();

	}



void StdBeep(void) {

	CCPR1L=BEEP_STD_FREQ/2;
	CCPR1H=BEEP_STD_FREQ/2;
	PR2=BEEP_STD_FREQ;

	T2CONbits.TMR2ON=1;
	Delay_S();
	T2CONbits.TMR2ON=0;
	}

void ErrorBeep(void) {

	CCPR1L=BEEP_STD_FREQ/2+30;
	CCPR1H=BEEP_STD_FREQ/2+30;
	PR2=BEEP_STD_FREQ+60;

	T2CONbits.TMR2ON=1;
	Delay_S();
	T2CONbits.TMR2ON=0;
	}


signed char EseguiComando(BYTE *c) {
	BYTE i;

	i=c[2]-'0';
	i += (c[1]-'0')*10;
	if(i>0 && i<=10) {
		i--;
		switch(c[0]) {
			case 'A':		// accendo
				statoLed[i]=LED_ACCESO;
				break;
			case 'S':		// spengo
				statoLed[i]=LED_SPENTO;
				break;
			case 'L':		// lampeggio
				statoLed[i]=LED_BLINK;
				break;
			default:
				return -1;
			}
		return 1;
		}
	else
		return 0;
	}


void lampLed(void) {

  LATA=0;
  LATB=0;
  LATD=0;
  LATE=0;
  StdBeep();
  LATA=255;		// tutto il blocco, non � un problema
  Delay_mS(250);
  LATA=0;
  LATB=255;
  StdBeep();
  LATB=0;
  LATD=255;
  Delay_mS(250);
  LATD=0;
  LATE=255;
	StdBeep();
//  Delay_mS(250);
  LATE=0;
	}
void AllLedOn(void){
m_Led1Bit=1;
m_Led2Bit=1;
m_Led3Bit=1;
m_Led4Bit=1;
m_Led5Bit=1;
m_Led6Bit=1;
m_Led7Bit=1;
m_Led8Bit=1;
m_Led9Bit=1;
m_Led10Bit=1;    
}
void AllLedOff(void){
m_Led1Bit=0;
m_Led2Bit=0;
m_Led3Bit=0;
m_Led4Bit=0;
m_Led5Bit=0;
m_Led6Bit=0;
m_Led7Bit=0;
m_Led8Bit=0;
m_Led9Bit=0;
m_Led10Bit=0;  
}
void AnimationLedsInit(void){
 
AllLedOff();
//STEP 0
m_Led6Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 1
m_Led6Bit=0;
m_Led1Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 2
m_Led1Bit=0;
m_Led7Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 3
m_Led7Bit=0;
m_Led3Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 4
m_Led3Bit=0;
m_Led4Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 5
m_Led4Bit=0;
m_Led8Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 6
m_Led8Bit=0;
m_Led9Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 7
m_Led9Bit=0;
m_Led10Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 8
m_Led10Bit=0;
m_Led2Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 9
m_Led2Bit=0;
m_Led5Bit=1;
Delay_mS(BLINK_INIT_MS);
//STEP 10
m_Led5Bit=0;
Delay_mS(BLINK_INIT_MS);
//STEP 11
num_all_blink=0;
while (num_all_blink<MAX_BLINK_CYCLE_INIT){
        ClrWdt();	
        AllLedOn();
        Delay_mS(BLINK_ALL_MS);
        AllLedOff();
        Delay_mS(BLINK_ALL_MS);
//        AllLedOn();
//        Delay_mS(BLINK_ALL_MS);
//        AllLedOff();
//        Delay_mS(BLINK_ALL_MS);
//        AllLedOn();
//        Delay_mS(BLINK_ALL_MS);
//        AllLedOff();
        ClrWdt();	
        num_all_blink++;
    }
 
}

void gestisciLed(void) {

// aggiorno situazione led, gestisce lampeggio
#ifndef DEBUG_VALENTI
		if(statoLed[0] == LED_SPENTO) {
			m_Led1Bit=0;
			}
		else if(statoLed[0] == LED_ACCESO) {
			m_Led1Bit=1;
			}
		else if(statoLed[0] == LED_BLINK) {
			m_Led1Bit=m_LedOBit;
			}
#endif
		if(statoLed[1] == LED_SPENTO) {
			m_Led2Bit=0;
			}
		else if(statoLed[1] == LED_ACCESO) {
			m_Led2Bit=1;
			}
		else if(statoLed[1] == LED_BLINK) {
			m_Led2Bit=m_LedOBit;
			}
		if(statoLed[2] == LED_SPENTO) {
			m_Led3Bit=0;
			}
		else if(statoLed[2] == LED_ACCESO) {
			m_Led3Bit=1;
			}
		else if(statoLed[2] == LED_BLINK) {
			m_Led3Bit=m_LedOBit;
			}
		if(statoLed[3] == LED_SPENTO) {
			m_Led4Bit=0;
			}
		else if(statoLed[3] == LED_ACCESO) {
			m_Led4Bit=1;
			}
		else if(statoLed[3] == LED_BLINK) {
			m_Led4Bit=m_LedOBit;
			}
		if(statoLed[4] == LED_SPENTO) {
			m_Led5Bit=0;
			}
		else if(statoLed[4] == LED_ACCESO) {
			m_Led5Bit=1;
			}
		else if(statoLed[4] == LED_BLINK) {
			m_Led5Bit=m_LedOBit;
			}
		if(statoLed[5] == LED_SPENTO) {
			m_Led6Bit=0;
			}
		else if(statoLed[5] == LED_ACCESO) {
			m_Led6Bit=1;
			}
		else if(statoLed[5] == LED_BLINK) {
			m_Led6Bit=m_LedOBit;
			}
		if(statoLed[6] == LED_SPENTO) {
			m_Led7Bit=0;
			}
		else if(statoLed[6] == LED_ACCESO) {
			m_Led7Bit=1;
			}
		else if(statoLed[6] == LED_BLINK) {
			m_Led7Bit=m_LedOBit;
			}
		if(statoLed[7] == LED_SPENTO) {
			m_Led8Bit=0;
			}
		else if(statoLed[7] == LED_ACCESO) {
			m_Led8Bit=1;
			}
		else if(statoLed[7] == LED_BLINK) {
			m_Led8Bit=m_LedOBit;
			}
		if(statoLed[8] == LED_SPENTO) {
			m_Led9Bit=0;
			}
		else if(statoLed[8] == LED_ACCESO) {
			m_Led9Bit=1;
			}
		else if(statoLed[8] == LED_BLINK) {
			m_Led9Bit=m_LedOBit;
			}
		if(statoLed[9] == LED_SPENTO) {
			m_Led10Bit=0;
			}
		else if(statoLed[9] == LED_ACCESO) {
			m_Led10Bit=1;
			}
		else if(statoLed[9] == LED_BLINK) {
			m_Led10Bit=m_LedOBit;
			}

		if(statoBuzzer == LED_SPENTO) {
			T2CONbits.TMR2ON=0;
			}
		else if(statoBuzzer == LED_ACCESO) {
			T2CONbits.TMR2ON=1;
			}
		else if(statoBuzzer == LED_BLINK) {
			T2CONbits.TMR2ON=m_LedOBit;
			}

	}
   
// spedizione frame  ------------------------------------------------------------
BYTE SpedisciFrame(WORD n) {
	BYTE i;
	WORD j;
	BYTE *p;
	static WORD oldN=0;
	static BYTE oldTasto=0;
#if MODO_TASTI==2
	static BYTE autorepeatDivider=0;
#elif MODO_TASTI==4
	static BYTE autorepeatDivider=0;
    WORD chksum= 0x0000;
    WORD maskbit=0x3FFF;
    WORD mask7keys=0x0000;
    WORD dataTosend=0x0000;    
#endif

#if MODO_TASTI==1

	ByteCnt=0;
	if(n != oldN) {		// qua no autorepeat
		for(i=1,j=1; i<17; i++,j<<=1) {
			if(n & j) {
				packetOut[0]=GetKBchar(i);
				ByteCnt=1;
				break;
				}
			}
		}

#elif MODO_TASTI==2

	ByteCnt=0;

	autorepeatDivider++;
	if(autorepeatDivider >= AUTOREPEAT_RATE) {
		autorepeatDivider=0;
		for(i=1,j=1; i<17; i++,j<<=1) {
			if(n & j) {
				packetOut[0]=GetKBchar(i);
				ByteCnt=1;
				break;
				}
			}
		}

#elif MODO_TASTI==3

	if(n>oldN) {		//mah.. dovrebbe andare?
		packetOut[0]=PREAMBOLO_TASTI_PRESSIONE;
		for(i=1,j=1; i<17; i++,j<<=1) {
			if(n & j) {
				packetOut[1]=GetKBchar(i);
				oldTasto=i;
				ByteCnt=2;
				break;
				}
			}
		if(i==16) {			// nessun tasto trovato
			ByteCnt=0;
			}
		}
	else if(n<oldN && oldTasto) {		//mah.. dovrebbe andare?
		packetOut[0]=PREAMBOLO_TASTI_RILASCIO;
		packetOut[1]=GetKBchar(oldTasto);
		oldTasto=0;
		ByteCnt=2;
		}
	else {
		ByteCnt=0;
		}
	
#elif MODO_TASTI==4
    autorepeatDivider++;
	if(autorepeatDivider >= AUTOREPEAT_RATE) {
        autorepeatDivider=0;
        ByteCnt=0;
        j=0;
        chksum= 0x0000;
        packetOut[j++]=STARTBYTE_TX_CU_LIGI_01;
        maskbit=0x007F;
        mask7keys=((n>>7) &  maskbit);//(n>>8);//
        dataTosend= (mask7keys);//(mask7keys | 0x0080);//create byte high to send as bitmap of first row of 7 keys of the keyboard
        packetOut[j++]=(BYTE)dataTosend;
        dataTosend=(n & maskbit);//(n & 0x00FF);//((n & maskbit)| 0x0080);//create byte low to send as bitmap of second row of 7 keys of  the keyboard
        packetOut[j++]=(BYTE)dataTosend;
        for (i=0;i< (LEN_FRAME_TX_CU_LIGI-2);i++){
          chksum+= packetOut[i];
        }
        dataTosend=((chksum>>8) & 0x00FF);
        packetOut[j++]=(BYTE)dataTosend;
        dataTosend=(0x00FF & chksum);
        packetOut[j++]=(BYTE)dataTosend;
        ByteCnt=1;
    }
#else
#error MODO_TASTI non definito correttamente
#endif

	oldN=n;


  ClrWdt();

	if(ByteCnt>0)
		CommStatus.FRAME_2SEND=1;		// pleonastico, di nuovo!

  if(CommStatus.FRAME_2SEND) {       // c'e' un frame da spedire..

//if RTS) {
			CommStatus.FRAME_2SEND=0;			// ...allora spedisco!
#if MODO_TASTI==4
            p=packetOut;
            for (i=0;i<LEN_FRAME_TX_CU_LIGI;i++){
                PutByte(*p++);
            }
#else
         	p=packetOut;

			do {
				PutByte(*p++);
				} while(--ByteCnt);

ComEndSend: ;
#endif
		
		}


}




// ---------------------------------------------------------------------
short int GetByte() {          // torna -1 (0xffff) se niente oppure il BYTE e ByteRec!!
	/*overlay*/ BYTE ByteRec;

	ClrWdt();

	if(!isRecv232())
		return -1;

	ByteRec=Buf232[Buf232PtrI++];
	Buf232PtrI &= (BUF_232_SIZE-1);				// max 32

	return ByteRec;
	}



#define PutByte0() PutByte(0)               // manda uno 0

BYTE PutByte(BYTE n) {                // manda un byte

	do {
		ClrWdt();
		} while(!PIR1bits.TXIF);			// aspetto che il buffer di TX sia libero (v. anche MandaHdr)

	TXREG=n;
		/*WriteUSART(n)*/

	do {
		ClrWdt();
		} while(Busy1USART() /*!TXSTAbits.TRMT*/);		// aspetto che finisca (per "sentire" collisione) FORSE TORNA "1" PRIMA di STOP bit!


//	Delay08();			// quindi... forse xche' TORNA "1" PRIMA di STOP bit!
									//	qui e' abb. inutile, xche' il loop ProcessIO/USB passa ogni circa 15uSec...
									//	pero' potrebbe servire in una vera Tx Back2Back


  return 1;
	}




// ---------------------------------------------------------------------

// ---------------------------------------------------------------------


#define Delay01() Delay_uS(1)			// TIME_GRANULARITY						; 1 bit-time

//Delays W microseconds (includes movlw, call, and return) @ 29MHz
void Delay_uS(BYTE uSec) {

	do {
		ClrWdt();			// 1
		Delay1TCY();			// TARARE!! @29MHz??
		Delay1TCY();
		Delay1TCY();
		Delay1TCY();
		} while(--uSec);

  //return             ; 1
	}



//Delays approx W milliseconds
void Delay_mS(BYTE mSec) {
	
		// usare Delay10KTCY();			// TARARE!! @29MHz??

	do {
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(200);
		Delay_uS(125);
		}	while(--mSec);


//	return;
	}




void Delay_S_(BYTE n) {				// circa n*100mSec

	do {
	  Delay_mS(97);
		} while(--n);
	}



// -------------------------------------------------------------------------------------
void EEscrivi_(SHORTPTR addr,BYTE n) {

	EEADR = (BYTE)addr;
	EEDATA=n;

	EECON1bits.EEPGD=0;		// Point to Data Memory
	EECON1bits.CFGS=0;		// Access EEPROM
	EECON1bits.WREN=1;

//	INTCONbits.GIE = 0;			// disattiva interrupt globali
	EECON2=0x55;		 // Write 55h
	EECON2=0xAA;		 // Write AAh
	EECON1bits.WR=1;									// abilita write.
	do {
		ClrWdt();
		} while(EECON1bits.WR);							// occupato ? 

//	INTCONbits.GIE = 1;			// attiva interrupt globali

	EECON1bits.WREN=0;								// disabilita write.
  }

BYTE EEleggi(SHORTPTR addr) {

	EEADR=(BYTE)addr;			// Address to read
	EECON1bits.EEPGD=0;		// Point to Data Memory
	EECON1bits.CFGS=0;		// Access EEPROM
	EECON1bits.RD=1;		// EE Read
	return EEDATA;				// W = EEDATA
	}


// ---------------------------------------------------------------------------------------

