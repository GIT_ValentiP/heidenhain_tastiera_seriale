
//#define DEBUG_VALENTI 1
//#define DEBUG_TEST 1			// attiva loop di prova () tenendo chiuso puls.6 al boot
#define BAUD_RATE 9600		// o 4800 o 19200 o 38400
#define MODO_TASTI   4			// 
/*MODO 1: I TASTI QUANDO PREMUTI MANDANO IL LORO CODICE.
MODO 2: I TASTI QUANDO PREMUTI MANDANO IL LORO CODICE OGNI XXXms FINO A CHE NON
VENGONO RILASCIATI.
MODO3: IL TASTO MANDA UN PREAMBOLO DI PRESSIONE + CODICE TASTO ALLA PRESSIONE E
UN PREAMBOLO DI RILASCIO + CODICE TASTO AL RILASCIO ( I PREAMBOLI SONO UGUALI PER
OGNI TASTO)
 MODO 4 :  TASTI PER CLIENTE HEIDENHAIN CON BITMAP TASTI PREMUTI REPETITION TIME 55ms .STREAM BYTE CON START BYTE e CHECKSUM A WORD 
 */
#define PREAMBOLO_TASTI_PRESSIONE '$'			// 
#define PREAMBOLO_TASTI_RILASCIO '%'			// 
#define AUTOREPEAT_RATE 1			// 1..8 => 125mS..1sec
#define STARTBYTE_TX_CU_LIGI_01  0x80
#if MODO_TASTI == 4
#define LEN_FRAME_TX_CU_LIGI     5   
#else
#define LEN_FRAME_TX_CU_LIGI     4
#endif
#define REPEAT_CYCLE_RX 3//10
#define REPEAT_CYCLE_MS (REPEAT_CYCLE_RX*17)//10
#define RS232_RX_TIME_SLOT_IN_NUM_LOOP   10 
#define RS232_TX_TIME_REPEATITION_IN_NUM_LOOP  1

typedef char * SHORTPTR;

#define TMR0BASE  (65536-23436)		//	TMR0 viaggia a 128presc*(5MHz (100nS)), io voglio un IRQ ogni 100mS 

#define TIME_GRANULARITY 208   // il tempo per 1 bit (208uS @ 4800baud)
// e agli altri? boh

#define BLINK_INIT_MS 100//150
#define BLINK_ALL_MS 200
#define MAX_BLINK_CYCLE_INIT 3	

#define PERIOD_BLINK_LED  250
#define PERIOD_BLINK_LED_ITERATION   (PERIOD_BLINK_LED/REPEAT_CYCLE_MS)

enum {
	BEEP_STD_FREQ=100,
	BEEP_LOW_FREQ=250,
	BEEP_HIGH_FREQ=50
	};

#define BUF_232_SIZE 8

struct COMM_STATUS
{
// errori/flag in CommStatus Seriale
	unsigned int FRAME_2SEND:1;         // c'e' da mandare un frame
	unsigned int WAIT_4ACK  :1;
	unsigned int FRAME_REC  :1;         // c'e' un frame ricevuto
	unsigned int COMM_OVRERR:1;			// overrun seriale 
	unsigned int COMM_OVL   :1;         // errore di overflow buffer in ricezione 
	unsigned int COMM_TOUT  :1;         // errore di timeout in ricezione frame
	unsigned int COMM_FRERR :1;         // errore di framing (START/STOP) in ricezione 
	unsigned int COMM_PERR  :1;         // errore di parita' in ricezione 
};


BYTE GetKBchar(BYTE );



// PORT A: 
#define Led1Bit		0
#define m_Led1Bit		LATAbits.LATA0

#define Puls15Bit		1
#define m_Puls15Bit		PORTAbits.RA1
#define Puls9Bit		2	
#define m_Puls9Bit		PORTAbits.RA2
#define Puls8Bit		3
#define m_Puls8Bit		PORTAbits.RA3
#define Puls7Bit		4
#define m_Puls7Bit		PORTAbits.RA4
#define Puls14Bit		5
#define m_Puls14Bit		PORTAbits.RA5


// port B:
#define Led3Bit		1
#define m_Led3Bit		LATBbits.LATB1
#define Led2Bit		6
#define m_Led2Bit		LATBbits.LATB6

#define Puls6Bit		0	
#define m_Puls6Bit		PORTBbits.RB0
#define Puls5Bit		2
#define m_Puls5Bit		PORTBbits.RB2
#define Puls4Bit		3	
#define m_Puls4Bit		PORTBbits.RB3
#define Puls3Bit		4
#define m_Puls3Bit		PORTBbits.RB4
#define Puls2Bit		5
#define m_Puls2Bit		PORTBbits.RB5
#define Puls1Bit		7
#define m_Puls1Bit		PORTBbits.RB7


// port C
#define Puls16Bit		5
#define m_Puls16Bit		PORTCbits.RC5
#define LedOBit		0
#define m_LedOBit		LATCbits.LATC0

#define BuzzBit			2		// PWM1
#define m_BuzzBit		PORTCbits.RC2

//#define RTSBit			0		// per bootloader??
//#define m_RTSBit		LATAbits.LATA0

#define BuzzVal		(1 << BuzzBit)
#define RTSVal		(1 << RTSBit)


// port D  (I/O)
#define Led5Bit		0
#define m_Led5Bit		LATDbits.LATD0
#define Led9Bit		3
#define m_Led9Bit		LATDbits.LATD3
#define Led10Bit		4
#define m_Led10Bit		LATDbits.LATD4
#define Led4Bit		7
#define m_Led4Bit		LATDbits.LATD7

#define Puls13Bit		1
#define m_Puls13Bit		PORTDbits.RD1
#define Puls12Bit		2
#define m_Puls12Bit		PORTDbits.RD2
#define Puls11Bit		5
#define m_Puls11Bit		PORTDbits.RD5
#define Puls10Bit		6
#define m_Puls10Bit		PORTDbits.RD6


#define Puls0Val (1 << Puls0Bit)
#define Puls1Val (1 << Puls1Bit)
#define Puls2Val (1 << Puls2Bit)
#define Puls3Val (1 << Puls3Bit)
#define Puls4Val (1 << Puls4Bit)
#define Puls5Val (1 << Puls5Bit)
#define Puls6Val (1 << Puls6Bit)
#define Puls7Val (1 << Puls7Bit)
#define Puls8Val (1 << Puls8Bit)
#define Puls9Val (1 << Puls9Bit)
#define Puls10Val (1 << Puls10Bit)
#define Puls11Val (1 << Puls11Bit)
#define Puls12Val (1 << Puls12Bit)
#define Puls13Val (1 << Puls13Bit)
#define Puls14Val (1 << Puls14Bit)
#define Puls15Val (1 << Puls15Bit)



#define Led0Val (1 << Led0Bit)
#define Led1Val (1 << Led1Bit)
#define Led2Val (1 << Led2Bit)
#define Led3Val (1 << Led3Bit)
#define Led4Val (1 << Led4Bit)
#define Led5Val (1 << Led5Bit)
#define Led6Val (1 << Led6Bit)
#define Led7Val (1 << Led7Bit)
#define Led8Val (1 << Led8Bit)
#define Led9Val (1 << Led9Bit)
#define Led10Val (1 << Led10Bit)


// port E 
#define Led6Bit		0
#define m_Led6Bit		LATEbits.LATE0	
#define Led7Bit		1
#define m_Led7Bit		LATEbits.LATE1	
#define Led8Bit		2
#define m_Led8Bit		LATEbits.LATE2


#define LedOVal		(1 << LedOBit)





#define CR           0xd
#define LF           0xa



#define FLASH_TIME   31





#define SERNUM      1000
#define VERNUMH     1
#define VERNUML     1

