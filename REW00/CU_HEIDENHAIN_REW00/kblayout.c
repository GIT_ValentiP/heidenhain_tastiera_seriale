// VERSIONE 

//------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------------------




/*********************************************************************
 *
 *                Microchip USB C18 Firmware - Mouse + tastiera USB
 *
 *********************************************************************
 * FileName:        kblayout.c
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18
 * Compiler:        C18 2.30.01+
 * Company:         Microchip Technology, Inc.
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;   Author:			Dario Greggio & Matteo Vallone
;		1/8/01: inizio
;		2/02: layout Mac
;		3/02: modificati nomi delle label tasti
;		6/05: versione C/Flash Pic18
;		7/11: versione 18F44K, terminalino RS232
 ********************************************************************/


/** I N C L U D E S **********************************************************/
#include <p18cxxx.h>
#include <delays.h>
#include "generictypedefs.h"
#include "kt-ed4-term.h"




/** V A R I A B L E S ********************************************************/


#pragma udata

rom BYTE *kbptr;
extern BYTE kbLayout;

#pragma romdata


rom BYTE tastoDummy=0;		// usato all'inizio (perche' ScanCode parte da 1) e per i tasti non-esistenti

// convenzione usata:

// //		da 1 a 239 e' un tasto semplice, che genera make/break code alla pressione/rilascio      *** AUTOREPEAT ***
// //		0		e' un tasto non usato.
// //		255 e' riservato (tasto non usato)


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------



rom BYTE tastoF1=0x05;    //F1
rom BYTE tastoF2=0x06;    //F2
rom BYTE tastoF3=0x04;    //F3
rom BYTE tastoF4=0x0c;    //F4
rom BYTE tastoF5=0x03;		//F5
rom BYTE tastoF6=0x0b;    //F6
rom BYTE tastoF7=0x83;    //F7
rom BYTE tastoF8=0x0a;    //F8
rom BYTE tastoF9=0x01;    //F9
rom BYTE tastoF10=0x09;		//F10
rom BYTE tastoF11=0x78;   //F11
rom BYTE tastoF12=0x07;   //F12


rom BYTE tasto1=0x16;	  	//1
rom BYTE tasto2=0x1e;  		//2
rom BYTE tasto3=0x26; 		//3
rom BYTE tasto4=0x25; 		//4
rom BYTE tasto5=0x2e; 		//5
rom BYTE tasto6=0x36; 		//6
rom BYTE tasto7=0x3d; 		//7
rom BYTE tasto8=0x3e; 		//8
rom BYTE tasto9=0x46; 		//9
rom BYTE tasto0=0x45; 		//0


rom BYTE tastoA='A';	  //A
rom BYTE tastoB='B';	  //B
rom BYTE tastoC='C';  	//C
rom BYTE tastoD='D';  	//D
rom BYTE tastoE='E';		//E
rom BYTE tastoF='F';  	//F
rom BYTE tastoG='G';  	//G
rom BYTE tastoH='H';  	//H
rom BYTE tastoI='I';  	//I
rom BYTE tastoJ='J'; 		//J
rom BYTE tastoK='K';  	//K
rom BYTE tastoL='L';  	//L
rom BYTE tastoM='M';  	//M
rom BYTE tastoN='N';  	//N
rom BYTE tastoO='O';	  //O
rom BYTE tastoP='P';		//P
rom BYTE tastoQ=0x15;			//Q
rom BYTE tastoR=0x2d;	  	//R
rom BYTE tastoS=0x1b;	  	//S
rom BYTE tastoT=0x2c;	  	//T
rom BYTE tastoU=0x3c;	  	//U
rom BYTE tastoV=0x2a;	  	//V
rom BYTE tastoW=0x1d;		  //W
rom BYTE tastoX=0x22;	  	//X
rom BYTE tastoY=0x35;	  	//Y
rom BYTE tastoZ=0x1a;	  	//Z


rom BYTE tastoESC=0x76;	    	//ESC
rom BYTE tastoTAB=0xd;	    	//TAB
rom BYTE tastoSpazio=0x29;		// SPACE
rom BYTE tastoEnter=0x5a;	  	//ENTER
rom BYTE tastoBackSpace=0x66;	//BKSPC


rom BYTE tastoMinore=0x61;		// < >


rom BYTE tastoCapsLock=0x58;	   //CAPS LOCK
rom BYTE tastoNumLock=0x77;		   //NUM LOCK
rom BYTE tastoScrollLock=0x7e;	 //SCROLL


rom BYTE tasto1Num=0x69;	            	//1 Tast
rom BYTE tasto2Num=0x72;            		//2 Tast
rom BYTE tasto3Num=0x7a;            		//3 Tast
rom BYTE tasto4Num=0x6b;	            	//4 Tast
rom BYTE tasto5Num=0x73;	            	//5 Tast
rom BYTE tasto6Num=0x74;	            	//6 Tast
rom BYTE tasto7Num=0x6c;	            	//7 Tast
rom BYTE tasto8Num=0x75;            		//8 Tast
rom BYTE tasto9Num=0x7d;	             	//9 Tast
rom BYTE tasto0Num=0x70;                //0 Tast 
rom BYTE tastoPuntoNum=0x71;	        	//. Tast.
rom BYTE tastoSlashNum[]={0xe0,0x4a};  	/// Tast
rom BYTE tastoAsteriscoNum=0x7c;    		//* Tast
rom BYTE tastoMenoNum=0x7b;	          	//- Tast
rom BYTE tastoPiuNum=0x79;	           	//+ Tast
rom BYTE tastoEnterNum[]={0xe0,0x5a};		//ENTER Tast.


rom BYTE tastoFrecciaSu[]={0xe0,0x75};		//U ARROW			
rom BYTE tastoFrecciaDX[]={0xe0,0x74};		//R ARROW
rom BYTE tastoFrecciaGiu[]={0xe0,0x72};		//D ARROW
rom BYTE tastoFrecciaSX[]={0xe0,0x6b};		//L ARROW


rom BYTE tastoLShift=0x12;	            	// L SHFT
rom BYTE tastoRShift=0x59;            		//R SHIFT
rom BYTE tastoLCtrl=0x14;		              //L CTRL
rom BYTE tastoRCtrl[]={0xE0,0x14};     		//R CTRL
rom BYTE tastoLAlt=0x11;	              	//L ALT
rom BYTE tastoAltGr[]={0xE0,0x11};	     	//R ALT GR
rom BYTE tastoLShiftCond=0x92;	            	// L SHFT condizionale basato su CAPS-LOCK (francia 2007)






const rom BYTE * rom KB_layout0[]= {
	&tastoDummy,			// serve xche' kbScanCode parte da 1...

// 
	
//colonna 0															  
	&tastoA,		     										//RB7
	&tastoB,		      									//RB5
	&tastoC,						    						//RB4
	&tastoD,						    						//RB3

	&tastoE,														//RB2
	&tastoF,														//RB0
	&tastoG,    												//RA4
	&tastoH,    												//RA3

	&tastoI,			  										//RA2
	&tastoJ,	        									//RD6
	&tastoK,   			   									//RD5
	&tastoL,    												//RD2

	&tastoM,   													//RD1
	&tastoN,      											//RA5
	&tastoO,	   												//RA1
	&tastoP					    								//RC5

	};



const rom BYTE * rom KB_layout1[]= {
	&tastoDummy, 			// serve xche' kbScanCode parte da 1...

	};

const rom BYTE * rom KB_layout2[]= {
	&tastoDummy, 			// serve xche' kbScanCode parte da 1...

	};

const rom BYTE * rom KB_layout3[]= {
	&tastoDummy, 			// serve xche' kbScanCode parte da 1...

	};



BYTE GetKBchar(BYTE n) {

	switch(kbLayout) {
		case 0:
			kbptr=KB_layout0[n];
			return *kbptr;
			break;

#if MAX_LAYOUT>1
		case 1:
			kbptr=KB_layout1[n];
			return *kbptr;
			break;

#if MAX_LAYOUT>2
		case 2:
			kbptr=KB_layout2[n];
			return *kbptr;
			break;
		case 3:
			kbptr=KB_layout3[n];
			return *kbptr;
			break;
#endif
#endif

		}
	}

BYTE GetKBcharNext(void) {

	return *++kbptr;
	}

BYTE GetKBcharPrev(void) {

	return *--kbptr;
	}







/** EOF kblayout.c *********************************************************/


